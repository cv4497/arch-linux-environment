#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias quartus='/home/s4iot/Program-files/intelFPGA_lite/20.1/quartus/bin/quartus --64bit'
alias gitlab='brave https://gitlab.com/users/sign_in'
alias canvas='brave https://iteso.instructure.com/'
alias biblio='brave https://biblio.iteso.mx'
alias calendar='firefox https://calendar.google.com/'
alias vbox='VirtualBox %U'
alias ..='cd ..'
alias gf='cd;cd Git'
alias doc='cd; cd Documents'
alias sublime='/opt/sublime_text/sublime_text %F'
alias editbash='emacs ~/.bashrc'
alias setbash='source ~/.bashrc'
alias vs='/opt/visual-studio-code/code --no-sandbox --unity-launch %F'
alias ep='export PATH="$HOME/.local/bin:$PATH"'
alias usb0_active='sudo chmod 666 dev/ttyUSB0'
alias jl='export PATH="$HOME/.local/bin:$PATH"; cd git; jupyter lab'
alias cls='clear'
alias gadd='git add *.pdf *.ipynb *.py'
alias sysu='sudo pacman -Sys'
alias sysi='sudo pacman -S'
alias sysr='sudo -v'
alias syspi='sudo pacman -Qe'
alias sysr='sudo pacman -R'
alias gclone='git clone git@gitlab.com:'
alias gaddc='git add *.c *.h'
alias gcmm='git commit -m '
alias gpsh='git push origin'
alias py='python'
alias mcux='env SWT_GTK3=0 /usr/local/mcuxpressoide-11.3.0_5222/ide/mcuxpressoide'
alias f='dolphin'
alias create_sll='ssh-keygen -t ed25519'
alias ssl_key='cat ~/.ssh/id_ed25519.pub'
alias k32='cd ; cd /home/s4iot/Git/k32-l2a-drivers/'
alias kairos='cd ; cd /home/s4iot/Git/kairos/'
alias rmlock='sudo rm /var/lib/pacman/db.lck'
alias gcl='cd; cd /home/s4iot/Git/scripts/; perl git.pl'
alias plasma_rst='kstart5 plasmashell'
alias plasma_kill='kquitapp5 plasmashell'
alias rmteamscache='rm -rf ~/.config/Microsoft'
export QSYS_ROOTDIR="/home/s4iot/intelFPGA_lite/20.1/quartus/sopc_builder/bin"
export PATH="$HOME/.local/bin:$PATH"


